# Standard Plus
Drupal comes with a few install profiles, including "Minimal", "Standard", and "Umami".

The "Standard plus" module aims to act as a quick, low-effort way to DL & enable the most common modules needed by most Drupal websites, but missing from core's install profiles.

## Installation
Install as you would normally install a contributed Drupal module. To get the most out of this module in particular, we recommend installing it using Composer. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Features
Initially, this module will simply download (via composer) & enable very popular Drupal modules.

Modules it will enable/download include:
<ul>
  <li>Pathauto</li>
  <li>Redirect</li>
  <li>Admin Toolbar</li>
</ul>

## Requests
Want another module (or feature) added? Open a Feature request issue!

For the module's project page, visit the
[project page](https://www.drupal.org/project/standard_plus).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/standard_plus).


## Supporting this Module
This module is contributed and maintained by the team at Debug Academy.

The following blurb helps to support this module's maintenance and development:
At Debug Academy, we offer live training classes including <a href="https://debugacademy.com/course/drupal-web-development" target="_blank">Drupal training for beginners, <a href="https://debugacademy.com/drupal-certification-training">Drupal Certification training</a> and more <a href="https://debugacademy.com/courses">web development training</a> courses.

## Maintainers

Current maintainers:

- [Ashraf Abed (ashrafabed)](https://www.drupal.org/u/ashrafabed)

Supporting organizations:

- [Debug Academy](https://www.drupal.org/debug-academy) Sponsored creation & maintenance of the module!
